const numToWord_input = document.getElementById("numToWord_input");
const convertButtonElement = document.getElementById("numToWord_btn");
const spanOutput = document.getElementById("output");

const _setIdentifier = [
	" ",
	" ",
	"Thousand , ",
	"Million , ",
	"Billion , ",
	"Trillion , ",
	"Quadrillion , ",
	"Quintillion , ",
	"Zillion , ",
];
const _ones = [
	"Zero ",
	"One ",
	"Two ",
	"Three ",
	"Four ",
	"Five ",
	"Six ",
	"Seven ",
	"Eight ",
	"Nine ",
];
const _teens = [
	"Ten ",
	"Eleven ",
	"Twelve ",
	"Thirteen ",
	"Fourteen ",
	"Fifteen ",
	"Sixteen ",
	"Seventeen ",
	"Eighteen ",
	"Nineteen ",
];

const _tens = [
	" ",
	"Ten ",
	"Twenty ",
	"Thirty ",
	"Forty ",
	"Fifty ",
	"Sixty ",
	"Seventy ",
	"Eighty ",
	"Ninety ",
];

let originalValue = "";
let originalLength = "";
let finalWord = "";
let wordOutput = "";
let newArray = [];
let niceSet = "";

const one_digit = (num) => {
	const ones_output = _ones[num];
	return ones_output;
};
const teen_digit = (num) => {
	const teenNum = num.slice(1);
	return _teens[teenNum];
};

const two_digit = (num) => {
	const quotient = Math.floor(num / 10);
	return _tens[quotient];
};

const three_digit = (num) => {
	let quotient = Math.floor(num / 100);
	return _ones[quotient];
};

const showOutput = (output) => {
	spanOutput.innerText = output;
	resetSpan();
};

const convertNumToWordHandler = (value) => {
	let newValue;
	const valueLength = value.length;
	if (valueLength === 3) {
		wordOutput += three_digit(value);
		wordOutput += "Hundred ";
		if (value % 100 === 0) {
			// showOutput(wordOutput);
			return;
		}
	} else if (valueLength === 2) {
		if (value >= 20 && value < 100) {
			wordOutput += two_digit(value);
			const modOfNum = value % 10;
			if (modOfNum === 0) {
				// showOutput(wordOutput);
				return;
			}
		} else if (value >= 10 && value < 20) {
			wordOutput += teen_digit(value);
			// showOutput(wordOutput);
			return;
		}
	} else if (valueLength === 1) {
		wordOutput += one_digit(value);
	}

	newValue = value.slice(1);
	if (!newValue) {
		// showOutput(wordOutput);
		return;
	}
	convertNumToWordHandler(newValue);
};

const resetSpan = () => {
	wordOutput = "";

	numToWord_input.focus();
	niceSet = "";
};

// const testSlice = (num) => {
// 	console.log(num);

// 	const newNum = num.slice(1);
// 	if (!newNum) {
// 		return;
// 	}
// 	testSlice(newNum);
// };

const mapNewArray = (array) => {
	let lengthOfArray = array.length;
	array.forEach((set, index) => {
		// const item = {
		// 	set,
		// 	index,
		// 	length: set.length,
		// 	origin: originalLength - index * 3,
		// };
		// console.log("map => ", item);
		const identity = _setIdentifier[lengthOfArray];
		const parseSet = parseInt(set);

		if (set > 0) {
			niceSet += set;
			convertNumToWordHandler(parseSet.toString());
			wordOutput += identity;
		}
		niceSet += ", ";
		lengthOfArray--;
	});
	showOutput(wordOutput);
};

const putInArray = (value, area, length) => {
	const slicedNum = value.slice(area, length);
	newArray.unshift(slicedNum);

	if (length > 3) {
		putInArray(value, area - 3, length - 3);
	} else {
		mapNewArray(newArray);
	}
};

const submitHandler = (e) => {
	e.preventDefault();
	originalValue = numToWord_input.value.replaceAll(",", "");
	originalLength = originalValue.length;

	resetSpan();
	newArray = [];

	if (originalValue.trim() !== 0) {
		putInArray(originalValue, -3, originalLength);
	}
};

const toLocaleStringHandler = (e) => {
	let value = e.target.value.replaceAll(",", "");
	let pattern = /^[0-9]*$/;
	let result = pattern.test(value);

	if (!result) {
		value = value.slice(0, -1);
	}

	const parseIntValue = BigInt(value);
	// if (!parseIntValue || isNaN(parseIntValue)) {
	// 	return;
	// }

	const localeStringValue = parseIntValue.toLocaleString();
	numToWord_input.value = localeStringValue;
};

numToWord_input.addEventListener("input", toLocaleStringHandler);
numToWord_input.addEventListener("click", () => {
	numToWord_input.value = "";
});
convertButtonElement.addEventListener("click", submitHandler);
